/*
 * Copyright (C) 2021  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import CoinGecko 1.0 // CryptoPrices

MainView {
    id: root
    objectName: "mainView"
    applicationName: "utcryptoprices.farkasdvd"
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Settings {
        id: settings

        property string appThemeName: ""
        property string defaultFiatCurrencyId: "usd"
        property string lastInputCurrencyId: "usd"
        property string lastOutputCurrencyId: "bitcoin"

        onAppThemeNameChanged: {
            root.theme.name = settings.appThemeName
        }

        onDefaultFiatCurrencyIdChanged: {
            CryptoPrices.vsCurrencyId = settings.defaultFiatCurrencyId
            CryptoPrices.update()
        }
    }

    PageStack {
        id: pageStack

        Page {
            id: mainPage

            header: PageHeader {
                id: header
                title: sections.actions[sections.selectedIndex].text

                extension: Sections {
                    id: sections
                    actions: [
                        Action {
                            text: i18n.tr("Prices")

                            onTriggered: {
                                header.trailingActionBar.actions = []
                                header.trailingActionBar.actions.push(settingsActionComponent.createObject())
                                bodyLoader.setSource(Qt.resolvedUrl("PricesPage.qml"))
                            }
                        },
                        Action {
                            text: i18n.tr("Portfolio")

                            onTriggered: {
                                header.trailingActionBar.actions = []
                                header.trailingActionBar.actions.push(settingsActionComponent.createObject())
                                header.trailingActionBar.actions.push(addActionComponent.createObject())
                                header.trailingActionBar.actions.push(importActionComponent.createObject())
                                bodyLoader.setSource(Qt.resolvedUrl("PortfolioPage.qml"))
                            }
                        },
                        Action {
                            text: i18n.tr("Currencies")

                            onTriggered: {
                                header.trailingActionBar.actions = []
                                header.trailingActionBar.actions.push(settingsActionComponent.createObject())
                                bodyLoader.setSource(Qt.resolvedUrl("CurrenciesPage.qml"))
                            }
                        }
                    ]
                }
            }

            Loader {
                id: bodyLoader
                anchors.top: header.bottom
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right
            }
        }

        Component.onCompleted: {
            pageStack.push(mainPage)
        }
    }

    Component {
        id: settingsActionComponent

        Action {
            text: i18n.tr("Settings")
            iconName: "settings"

            onTriggered: {
                pageStack.push(Qt.resolvedUrl("SettingsPage.qml"));
            }
        }
    }

    Component {
        id: addActionComponent

        Action {
            text: i18n.tr("Add trade")
            iconName: "add"

            onTriggered: {
                pageStack.push(Qt.resolvedUrl("AddTradePage.qml"));
            }
        }
    }

    Component {
        id: importActionComponent

        Action {
            text: i18n.tr("Import trade")
            iconName: "import"

            onTriggered: {
                pageStack.push(Qt.resolvedUrl("ImportTradePage.qml"));
            }
        }
    }
}
