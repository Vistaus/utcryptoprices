/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import CoinGecko 1.0 // Trades, Currencies

Page {
    id: _root

    property var currency

    header: PageHeader {
        id: _header
        title: i18n.tr("%1 trades").arg(_root.currency.name)
        trailingActionBar.actions: [
            Action {
                text: i18n.tr("Add trade")
                iconName: "add"

                onTriggered: {
                    pageStack.push(Qt.resolvedUrl("AddTradePage.qml"))
                }
            }
        ]
    }

    UbuntuListView {
        id: _body
        anchors.top: _header.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        pullToRefresh.enabled: false
        currentIndex: -1
        clip: true
        model: Trades
        delegate: GeneralListItem {
            isVisible: (_root.currency.id == trade.outputCurrencyId) || (_root.currency.id == trade.inputCurrencyId)
            titleText: "+%1 %2".arg(trade.outputVolume).arg(Currencies.get(trade.outputCurrencyId).symbol)
            subtitleText: "-%1 %2".arg(trade.inputVolumeWithFee).arg(Currencies.get(trade.inputCurrencyId).symbol)
            iconSource: Currencies.get(trade.outputCurrencyId).icon
            action: Action {
                onTriggered: {
                    pageStack.push(Qt.resolvedUrl("EditTradePage.qml"), { trade: trade })
                }
            }
            leadingActions: ListItemActions {
                actions: [
                    Action {
                        iconName: "delete"

                        onTriggered: {
                            var popup = PopupUtils.open(Qt.resolvedUrl("DeleteConfirmationDialog.qml"), root,
                                        { title: i18n.tr("Delete trade?") })

                            popup.deleted.connect(function() {
                                Trades.deleteTrade(trade.id)
                            })
                        }
                    }
                ]
            }
        }
    }
}
