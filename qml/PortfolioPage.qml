/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import CoinGecko 1.0 // Portfolio, CryptoPrices, Currencies

UbuntuListView {
    id: _root
    anchors.fill: parent
    pullToRefresh.enabled: false
    currentIndex: -1
    clip: true
    model: Portfolio
    delegate: GeneralListItem {
        titleText: "%1 %2".arg(volume).arg(currency.symbol)
        subtitleText: calculateValue()
        iconSource: currency.icon
        action: Action {
            onTriggered: {
                pageStack.push(Qt.resolvedUrl("TradesPage.qml"), { currency: currency })
            }
        }

        function calculateValue() {
            // only for crypto currencies
            if(currency.type == 1) {
                var value = CryptoPrices.getFixedValue(currency.id, volume)
                var currencySymbol = Currencies.get(CryptoPrices.vsCurrencyId).symbol

                return "%1 %2".arg(value).arg(currencySymbol)
            }

            return null
        }

        Connections {
            target: CryptoPrices

            onUpdatingChanged: {
                if(!CryptoPrices.updating) {
                    subtitleText = calculateValue()
                }
            }
        }
    }

    Component.onCompleted: {
        Portfolio.initialize()
    }
}
