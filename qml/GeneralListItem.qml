/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3

ListItem {
    id: _root
    height: isVisible ? _content.height : units.gu(0)

    property string titleText
    property string subtitleText
    property alias iconSource: _icon.source
    property alias iconName: _icon.name
    property alias isProgressionVisible: _progression.visible
    property bool isVisible: true

    ListItemLayout {
        id: _content
        title.text: _root.titleText
        subtitle.text: _root.subtitleText

        Icon {
            id: _icon
            SlotsLayout.overrideVerticalPositioning: true
            SlotsLayout.position: SlotsLayout.Leading
            anchors.verticalCenter: parent.verticalCenter
            width: units.gu(4)
            height: width
        }

        ProgressionSlot {
            id: _progression
            SlotsLayout.overrideVerticalPositioning: true
            SlotsLayout.position: SlotsLayout.Trailing
            anchors.verticalCenter: parent.verticalCenter
        }
    }
}
