/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
    id: _root
    title: "Bitcoin"

    Icon {
        source: "assets/donate.png"
    }

    Label {
        id: _address
        text: "bc1q808dm47t705x6t6mfn3lxzmsdru4y779sq3csz"
        wrapMode: Text.WrapAnywhere
        horizontalAlignment: Text.AlignHCenter
    }

    Button {
        id: _copyButton
        text: i18n.tr("Copy to clipboard")

        onClicked: {
            Clipboard.push(_address.text)
            _copyButton.text = i18n.tr("Copied")
            _copyButton.enabled = false
        }
    }

    Button {
        text: i18n.tr("Cancel")

        onClicked: {
            PopupUtils.close(_root)
        }
    }
}
