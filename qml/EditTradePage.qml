/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import CoinGecko 1.0 // Trades, Currencies

Page {
    id: _root

    property var trade

    header: PageHeader {
        id: _header
        title: i18n.tr("Edit trade")

        trailingActionBar.actions: [
            Action {
                text: i18n.tr("Save trade")
                iconName: "ok"

                onTriggered: {
                    var tradeId = _root.trade.id
                    var inputCurrencyId = inputItem.currency.id
                    var inputVolume = parseFloat(inputItem.volume.length > 0 ? inputItem.volume : "0")
                    var outputCurencyId = outputItem.currency.id
                    var outputVolume = parseFloat(outputItem.volume.length > 0 ? outputItem.volume : "0")
                    var fee = parseFloat(feeItem.volume.length > 0 ? feeItem.volume : "0")
                    var date = dateItem.date
                    var note = noteItem.text

                    Trades.updateTrade(tradeId, inputCurrencyId, inputVolume, outputCurencyId, outputVolume, fee, date, note)
                    pageStack.pop()
                }
            }
        ]
    }

    Flickable {
        anchors.top: _header.bottom
        anchors.bottom: _osk.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: units.gu(2)
        contentHeight: content.height

        Column {
            id: content
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: units.gu(2)

            VolumeInputField {
                id: inputItem
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                currency: Currencies.get(_root.trade.inputCurrencyId)
                volumePlaceholder: i18n.tr("Input volume")
                volume: _root.trade.inputVolume

                onCurrencyClicked: {
                    var popup = PopupUtils.open(Qt.resolvedUrl("EditCurrencyDialog.qml"), _root,
                                { oldCurrency: inputItem.currency })

                    popup.selected.connect(function(newCurrency) {
                        inputItem.currency = newCurrency
                    })
                }
            }

            VolumeInputField {
                id: outputItem
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                currency: Currencies.get(_root.trade.outputCurrencyId)
                volumePlaceholder: i18n.tr("Output volume")
                volume: _root.trade.outputVolume

                onCurrencyClicked: {
                    var popup = PopupUtils.open(Qt.resolvedUrl("EditCurrencyDialog.qml"), _root,
                                { oldCurrency: outputItem.currency })

                    popup.selected.connect(function(newCurrency) {
                        outputItem.currency = newCurrency
                    })
                }
            }

            VolumeInputField {
                id: feeItem
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                currency: inputItem.currency
                currencyEditEnabled: false
                volumePlaceholder: i18n.tr("Fee")
                volume: _root.trade.fee
            }

            ListItem {
                id: dateItem
                height: dateItemContent.height
                divider.visible: false
                action: Action {
                    onTriggered: {
                        var popup = PopupUtils.open(Qt.resolvedUrl("EditDateDialog.qml"), _root,
                                    { oldDate: dateItem.date })

                        popup.selected.connect(function(newDate) {
                            dateItem.date = newDate
                        })
                    }
                }

                property var date: _root.trade.date

                ListItemLayout {
                    id: dateItemContent
                    title.text: i18n.tr("Date")
                    subtitle.text: Qt.formatDate(dateItem.date, Qt.LocalDate)

                    Icon {
                        SlotsLayout.overrideVerticalPositioning: true
                        SlotsLayout.position: SlotsLayout.Leading
                        anchors.verticalCenter: parent.verticalCenter
                        width: units.gu(3)
                        height: width
                        name: "preferences-system-time-symbolic"
                    }

                    ProgressionSlot {
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }
            }

            TextArea {
                id: noteItem
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                height: units.gu(10)
                placeholderText: i18n.tr("Note")
                text: _root.trade.note
            }
        }
    }

    Item {
        id: _osk
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: Qt.inputMethod.keyboardRectangle.height
    }
}
