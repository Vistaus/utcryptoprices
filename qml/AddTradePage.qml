/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import CoinGecko 1.0 // Trades, Currencies

Page {
    id: _root

    header: PageHeader {
        id: _header
        title: i18n.tr("Add trade")

        trailingActionBar.actions: [
            Action {
                text: i18n.tr("Save trade")
                iconName: "ok"

                onTriggered: {
                    var inputCurrencyId = _inputItem.currency.id
                    var inputVolume = parseFloat(_inputItem.volume.length > 0 ? _inputItem.volume : "0")
                    var outputCurencyId = _outputItem.currency.id
                    var outputVolume = parseFloat(_outputItem.volume.length > 0 ? _outputItem.volume : "0")
                    var fee = parseFloat(_feeItem.volume.length > 0 ? _feeItem.volume : "0")
                    var date = _dateItem.date
                    var note = _noteItem.text

                    settings.lastInputCurrencyId = inputCurrencyId
                    settings.lastOutputCurrencyId = outputCurencyId

                    Trades.addTrade(inputCurrencyId, inputVolume, outputCurencyId, outputVolume, fee, date, note)
                    pageStack.pop()
                }
            }
        ]
    }

    Flickable {
        anchors.top: _header.bottom
        anchors.bottom: _osk.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: units.gu(2)
        contentHeight: _content.height

        Column {
            id: _content
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: units.gu(2)

            VolumeInputField {
                id: _inputItem
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                currency: Currencies.get(settings.lastInputCurrencyId)
                volumePlaceholder: i18n.tr("Input volume")

                onCurrencyClicked: {
                    var popup = PopupUtils.open(Qt.resolvedUrl("EditCurrencyDialog.qml"), _root,
                                { oldCurrency: _inputItem.currency })

                    popup.selected.connect(function(newCurrency) {
                        _inputItem.currency = newCurrency
                    })
                }
            }

            VolumeInputField {
                id: _outputItem
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                currency: Currencies.get(settings.lastOutputCurrencyId)
                volumePlaceholder: i18n.tr("Output volume")

                onCurrencyClicked: {
                    var popup = PopupUtils.open(Qt.resolvedUrl("EditCurrencyDialog.qml"), _root,
                                { oldCurrency: _outputItem.currency })

                    popup.selected.connect(function(newCurrency) {
                        _outputItem.currency = newCurrency
                    })
                }
            }

            VolumeInputField {
                id: _feeItem
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                currency: _inputItem.currency
                currencyEditEnabled: false
                volumePlaceholder: i18n.tr("Fee")
            }

            ListItem {
                id: _dateItem
                height: _dateItemContent.height
                divider.visible: false
                action: Action {
                    onTriggered: {
                        var popup = PopupUtils.open(Qt.resolvedUrl("EditDateDialog.qml"), _root,
                                    { oldDate: _dateItem.date })

                        popup.selected.connect(function(newDate) {
                            _dateItem.date = newDate
                        })
                    }
                }

                property var date: new Date()

                ListItemLayout {
                    id: _dateItemContent
                    title.text: i18n.tr("Date")
                    subtitle.text: Qt.formatDate(_dateItem.date, Qt.LocalDate)

                    Icon {
                        SlotsLayout.overrideVerticalPositioning: true
                        SlotsLayout.position: SlotsLayout.Leading
                        anchors.verticalCenter: parent.verticalCenter
                        width: units.gu(3)
                        height: width
                        name: "preferences-system-time-symbolic"
                    }

                    ProgressionSlot {
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }
            }

            TextArea {
                id: _noteItem
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                height: units.gu(10)
                placeholderText: i18n.tr("Note")
            }
        }
    }

    Item {
        id: _osk
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: Qt.inputMethod.keyboardRectangle.height
    }
}
