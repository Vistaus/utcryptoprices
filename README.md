# UT Crypto Prices

Check cryptocurrency prices and track your portfolio.

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/utcryptoprices.farkasdvd)

## Description

Based on CoinGecko's free and public API (**limited to 50 requests per minute**).

The portfolio data is saved only locally in an SQLite database: `~/.local/share/utcryptoprices.farkasdvd/utcryptoprices.db`.

## Donate

### Bitcoin

![Bitcoin](assets/donate.png)

`bc1q808dm47t705x6t6mfn3lxzmsdru4y779sq3csz`

## License

Copyright (C) 2021  David Farkas

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
