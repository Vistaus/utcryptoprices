/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tradestable.h"
#include "trade.h"

#include <QDebug>
#include <QQmlEngine>

TradesTable::TradesTable()
    : Table("trades", {
          Column{"id", "integer primary key"},
          Column{"in_currency_id", "text"},
          Column{"in_volume", "double"},
          Column{"out_currency_id", "text"},
          Column{"out_volume", "double"},
          Column{"fee", "double"},
          Column{"date", "text"},
          Column{"note", "text"}
      })
    , m_trades()
{
    // intentionally empty
}

TradesTable::~TradesTable()
{
    for(Trade* trade : m_trades)
    {
        delete trade;
    }
}

bool TradesTable::initialize()
{
    bool result = true;

    result = result && addRow({"usd", 30000, "bitcoin", 1, 90, QDate(2022, 1, 17), "maker"});

    if(result)
    {
        qDebug() << "Initialized table" << name();
    }

    return result;
}

bool TradesTable::addRow(const QVariantList& values)
{
    bool result = false;

    QStringList column_names = columnNamesExceptPrimaryKey();

    if(values.size() == column_names.size())
    {
        const QString query_template = "insert into %1 (%2) values (?, ?, ?, ?, ?, ?, ?)";
        QSqlQuery query;

        query.prepare(query_template.arg(name()).arg(column_names.join(", ")));
        query.addBindValue(values.at(0).toString());
        query.addBindValue(values.at(1).toDouble());
        query.addBindValue(values.at(2).toString());
        query.addBindValue(values.at(3).toDouble());
        query.addBindValue(values.at(4).toDouble());
        query.addBindValue(values.at(5).toDate().toString(Qt::LocalDate));
        query.addBindValue(values.at(6).toString());

        result = execute(query);
    }
    else
    {
        qDebug() << "Columns vs values mismatch";
    }

    return result;
}

bool TradesTable::updateRow(const QVariantList& values)
{
    bool result = false;

    QStringList column_names = columnNames();

    if(values.size() == column_names.size())
    {
        QString query_template = "update %1 set %3 = '%11', %4 = %12, %5 = '%13', %6 = %14, %7 = %15, %8 = '%16', %9 = '%17' where %2 = %10";
        QSqlQuery query;

        query_template = query_template.arg(name());

        for(const QString& column_name : column_names)
        {
            query_template = query_template.arg(column_name);
        }

        query_template = query_template.arg(values.at(0).toInt());
        query_template = query_template.arg(values.at(1).toString());
        query_template = query_template.arg(values.at(2).toDouble());
        query_template = query_template.arg(values.at(3).toString());
        query_template = query_template.arg(values.at(4).toDouble());
        query_template = query_template.arg(values.at(5).toDouble());
        query_template = query_template.arg(values.at(6).toDate().toString(Qt::LocalDate));
        query_template = query_template.arg(values.at(7).toString());

        query.prepare(query_template);

        result = execute(query);
    }
    else
    {
        qDebug() << "Columns vs values mismatch";
    }

    return result;
}

bool TradesTable::deleteRow(const QVariant& trade_id)
{
    const QString query_template = "delete from %1 where %2 = %3";
    QSqlQuery query;

    query.prepare(query_template.arg(name()).arg(primaryKeyName()).arg(trade_id.toInt()));

    return execute(query);
}

bool TradesTable::load()
{
    bool result = false;
    const QString query_template = "select * from %1";
    QSqlQuery query;

    query.prepare(query_template.arg(name()));
    m_trades.clear();

    if(execute(query))
    {
        qDebug() << "Loaded table" << name();

        while(query.next())
        {
            const int id = query.value(0).toInt();
            const QString input_currency_id = query.value(1).toString();
            const double input_volume = query.value(2).toDouble();
            const QString output_currency_id = query.value(3).toString();
            const double output_volume = query.value(4).toDouble();
            const double fee = query.value(5).toDouble();
            const QDate date = QDate::fromString(query.value(6).toString(), Qt::LocalDate);
            const QString note = query.value(7).toString();

            Trade* trade = new Trade(id, input_currency_id, input_volume, output_currency_id, output_volume, fee, date, note);
            QQmlEngine::setObjectOwnership(trade, QQmlEngine::CppOwnership);

            m_trades.push_back(trade);
        }

        result = true;
    }

    return result;
}

Trade* TradesTable::getTrade(const int id) const
{
    Trade* result = nullptr;

    for(Trade* trade : m_trades)
    {
        if(trade->id() == id)
        {
            result = trade;
            break;
        }
    }

    return result;
}

QList<Trade*> TradesTable::getTrades() const
{
    return m_trades;
}
