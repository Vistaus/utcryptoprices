/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"

#include <QDebug>
#include <QStandardPaths>
#include <QSqlError>

Database* Database::getInstance()
{
    static Database* instance = nullptr;

    if(instance == nullptr)
    {
        instance = new Database;
    }

    return instance;
}

Database::~Database()
{
    m_db.close();
}

Currency* Database::getCurrency(const QString& id) const
{
    return m_currencies_table.getCurrency(id);
}

QList<Currency*> Database::getCurrencies() const
{
    return m_currencies_table.getCurrencies();
}

QList<Currency*> Database::getCryptoCurrencies() const
{
    return m_currencies_table.getCryptoCurrencies();
}

QList<Currency*> Database::getFiatCurrencies() const
{
    return m_currencies_table.getFiatCurrencies();
}

Trade* Database::getTrade(const int id) const
{
    return m_trades_table.getTrade(id);
}

QList<Trade*> Database::getTrades() const
{
    return m_trades_table.getTrades();
}

bool Database::insertTrade(const QString& input_currency_id,
                           const double input_volume,
                           const QString& output_currency_id,
                           const double output_volume,
                           const double fee,
                           const QDate& date,
                           const QString& note)
{
    bool result = true;

    result = result && m_db.open();
    result = result && m_trades_table.addRow({input_currency_id, input_volume, output_currency_id, output_volume, fee, date, note});
    result = result && m_trades_table.load();

    m_db.close();

    if(result)
    {
        emit tradesChanged();
    }

    return result;
}

bool Database::updateTrade(const int trade_id,
                           const QString& input_currency_id,
                           const double input_volume,
                           const QString& output_currency_id,
                           const double output_volume,
                           const double fee,
                           const QDate& date,
                           const QString& note)
{
    bool result = true;

    result = result && m_db.open();
    result = result && m_trades_table.updateRow({trade_id, input_currency_id, input_volume, output_currency_id, output_volume, fee, date, note});
    result = result && m_trades_table.load();

    m_db.close();

    if(result)
    {
        emit tradesChanged();
    }

    return result;
}

bool Database::deleteTrade(const int trade_id)
{
    bool result = true;

    result = result && m_db.open();
    result = result && m_trades_table.deleteRow(trade_id);
    result = result && m_trades_table.load();

    m_db.close();

    if(result)
    {
        emit tradesChanged();
    }

    return result;
}

Database::Database()
    : QObject()
    , m_db()
    , m_db_driver("QSQLITE")
    , m_db_dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation))
    , m_db_name("utcryptoprices.db")
    , m_currencies_table()
    , m_trades_table()
{
    m_db = QSqlDatabase::addDatabase(m_db_driver);

    if(m_db.isValid())
    {
        m_db_dir.mkpath(m_db_dir.path());
        m_db.setDatabaseName(m_db_dir.filePath(m_db_name));

        if(m_db.open())
        {
            qDebug() << m_db_name << "location:" << m_db_dir.path();

            prepareTables();

            m_db.close();
        }
        else
        {
            qDebug() << "Failed to open" << m_db_name << ", reason:" << m_db.lastError().databaseText();
        }
    }
    else
    {
        qDebug() << m_db_driver << "is not supported";
    }
}

void Database::prepareTables()
{
    const QStringList tables = m_db.tables();

    if(!tables.contains(m_currencies_table.name()))
    {
        m_currencies_table.create();
        m_currencies_table.initialize();
    }

    if(!tables.contains(m_trades_table.name()))
    {
        m_trades_table.create();
        m_trades_table.initialize();
    }

    m_currencies_table.load();
    m_trades_table.load();
}
