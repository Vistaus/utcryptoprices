/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CURRENCY_H
#define CURRENCY_H

#include <QObject>
#include <QString>

class Currency: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id NOTIFY idChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString symbol READ symbol WRITE setSymbol NOTIFY symbolChanged)
    Q_PROPERTY(QString icon READ icon WRITE setIcon NOTIFY iconChanged)
    Q_PROPERTY(int type READ type WRITE setType NOTIFY typeChanged)

public:
    enum Type
    {
        TypeUndefined,
        TypeCrypto,
        TypeFiat
    };

    Currency();
    Currency(const QString& id,
             const QString& name,
             const QString& symbol,
             const QString& icon,
             const int type);

    const QString& id() const;
    void setId(const QString& id);
    const QString& name() const;
    void setName(const QString& name);
    const QString& symbol() const;
    void setSymbol(const QString& symbol);
    const QString& icon() const;
    void setIcon(const QString& icon);
    int type() const;
    void setType(const int type);

signals:
    void idChanged();
    void nameChanged();
    void symbolChanged();
    void iconChanged();
    void typeChanged();

private:
    QString m_id;
    QString m_name;
    QString m_symbol;
    QString m_icon;
    Currency::Type m_type;
};

#endif // CURRENCY_H
