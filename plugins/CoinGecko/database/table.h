/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TABLE_H
#define TABLE_H

#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>
#include <QVariantList>
#include <QSqlQuery>

struct Column
{
    QString m_name;
    QString m_type;
};

class Table: public QObject
{
public:
    Table(const QString& name, const QList<Column>& columns);
    virtual ~Table() = default;

    const QString& name() const;
    int columnCount() const;
    const QString& primaryKeyName() const;
    const QStringList& columnNames() const;
    const QStringList& columnNamesExceptPrimaryKey() const;

    bool execute(QSqlQuery& query);
    bool create();
    virtual bool initialize() = 0;
    virtual bool addRow(const QVariantList& values) = 0;
    virtual bool updateRow(const QVariantList& values) = 0;
    virtual bool deleteRow(const QVariant& id) = 0;
    virtual bool load() = 0;

private:
    const QString m_name;
    const QList<Column> m_columns; // needs to be list, QHash doesn't preserve the order
    QString m_primary_key_name;
    QStringList m_column_names;
    QStringList m_column_names_except_primary_key;
};

#endif // TABLE_H
