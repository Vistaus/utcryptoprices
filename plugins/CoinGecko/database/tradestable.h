/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRADESTABLE_H
#define TRADESTABLE_H

#include "table.h"

class Trade;

class TradesTable: public Table
{
public:
    TradesTable();
    ~TradesTable();

    bool initialize() override;
    bool addRow(const QVariantList& values) override;
    bool updateRow(const QVariantList& values) override;
    bool deleteRow(const QVariant& trade_id) override;
    bool load() override;
    Trade* getTrade(const int id) const;
    QList<Trade*> getTrades() const;

private:
    QList<Trade*> m_trades;
};

#endif // TRADESTABLE_H
