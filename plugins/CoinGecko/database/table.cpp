/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "table.h"

#include <QDebug>
#include <QSqlError>

Table::Table(const QString& name, const QList<Column>& columns)
    : QObject()
    , m_name(name)
    , m_columns(columns)
    , m_primary_key_name()
    , m_column_names()
    , m_column_names_except_primary_key()
{
    for(const auto& column : m_columns)
    {
        m_column_names.push_back(column.m_name);

        if(column.m_type.contains("primary key"))
        {
            m_primary_key_name = column.m_name;
        }
        else
        {
            m_column_names_except_primary_key.push_back(column.m_name);
        }
    }
}

const QString& Table::name() const
{
    return m_name;
}

int Table::columnCount() const
{
    return m_columns.size();
}

const QString& Table::primaryKeyName() const
{
    return m_primary_key_name;
}

const QStringList& Table::columnNames() const
{
    return m_column_names;
}

const QStringList& Table::columnNamesExceptPrimaryKey() const
{
    return m_column_names_except_primary_key;
}

bool Table::execute(QSqlQuery& query)
{
    const bool result = query.exec();

    if(!result)
    {
        qDebug() << query.lastQuery() << "failed, reason:" << query.lastError().databaseText();
    }

    return result;
}

bool Table::create()
{
    bool result = false;
    const QString query_template = "create table %1 (%2)";
    QSqlQuery query;
    QStringList columns;

    for(const auto& column : m_columns)
    {
        columns.push_back(column.m_name + " " + column.m_type);
    }

    query.prepare(query_template.arg(m_name).arg(columns.join(", ")));

    if(execute(query))
    {
        qDebug() << "Created table" << m_name;

        result = true;
    }

    return result;
}
