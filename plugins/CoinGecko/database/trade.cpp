/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "trade.h"

#include <QLocale>

Trade::Trade()
    : Trade(0, QString(), 0.0, QString(), 0.0, 0.0, QDate(), QString())
{
    // intentionally empty
}

Trade::Trade(const int id,
             const QString& input_currency_id,
             const double input_volume,
             const QString& output_currency_id,
             const double output_volume,
             const double fee,
             const QDate& date,
             const QString& note )
    : QObject()
    , m_id(id)
    , m_input_currency_id(input_currency_id)
    , m_input_volume(input_volume)
    , m_output_currency_id(output_currency_id)
    , m_output_volume(output_volume)
    , m_fee(fee)
    , m_date(date)
    , m_note(note)
{
    // intentionally empty
}

int Trade::id() const
{
    return m_id;
}

void Trade::setId(const int id)
{
    if(id != m_id)
    {
        m_id = id;
        emit idChanged();
    }
}

const QString& Trade::inputCurrencyId() const
{
    return m_input_currency_id;
}

void Trade::setInputCurrencyId(const QString& currency_id)
{
    if(currency_id != m_input_currency_id)
    {
        m_input_currency_id = currency_id;
        emit inputCurrencyIdChanged();
    }
}

double Trade::inputVolume() const
{
    return m_input_volume;
}

QString Trade::fixedInputVolume() const
{
    return QString::number(m_input_volume, 'f', QLocale::FloatingPointShortest);
}

QString Trade::fixedInputVolumeWithFee() const
{
    return QString::number(m_input_volume + m_fee, 'f', QLocale::FloatingPointShortest);
}

void Trade::setInputVolume(const double volume)
{
    if(volume != m_input_volume)
    {
        m_input_volume = volume;
        emit inputVolumeChanged();
        emit inputVolumeWithFeeChanged();
    }
}

const QString& Trade::outputCurrencyId() const
{
    return m_output_currency_id;
}

void Trade::setOutputCurrencyId(const QString& currency_id)
{
    if(currency_id != m_output_currency_id)
    {
        m_output_currency_id = currency_id;
        emit outputCurrencyIdChanged();
    }
}

double Trade::outputVolume() const
{
    return m_output_volume;
}

QString Trade::fixedOutputVolume() const
{
    return QString::number(m_output_volume, 'f', QLocale::FloatingPointShortest);
}

void Trade::setOutputVolume(const double volume)
{
    if(volume != m_output_volume)
    {
        m_output_volume = volume;
        emit outputVolumeChanged();
    }
}

double Trade::fee() const
{
    return m_fee;
}

QString Trade::fixedFee() const
{
    return QString::number(m_fee, 'f', QLocale::FloatingPointShortest);
}

void Trade::setFee(const double fee)
{
    if(fee != m_fee)
    {
        m_fee = fee;
        emit feeChanged();
        emit inputVolumeWithFeeChanged();
    }
}

const QDate& Trade::date() const
{
    return m_date;
}

void Trade::setDate(const QDate& date)
{
    if(date != m_date)
    {
        m_date = date;
        emit dateChanged();
    }
}

const QString& Trade::note() const
{
    return m_note;
}

void Trade::setNote(const QString& note)
{
    if(note != m_note)
    {
        m_note = note;
        emit noteChanged();
    }
}
