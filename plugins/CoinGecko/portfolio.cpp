/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "portfolio.h"
#include "database/currency.h"
#include "database/trade.h"
#include "database/database.h"

#include <QDebug>

Portfolio::Portfolio()
    : QAbstractListModel()
    , m_items()
{
    // intentionally empty
}

void Portfolio::initialize()
{
    if(m_items.isEmpty())
    {
        handleTradesChanged();
    }
    else
    {
        qDebug() << "Already initialized";
    }

    connect(Database::getInstance(), &Database::tradesChanged, this, &Portfolio::handleTradesChanged);
}

int Portfolio::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return m_items.size();
}

QHash<int, QByteArray> Portfolio::roleNames() const
{
    QHash<int, QByteArray> result;

    result[RoleCurrency] = "currency";
    result[RoleVolume] = "volume";

    return result;
}

QVariant Portfolio::data(const QModelIndex& index, const int role) const
{
    QVariant result;

    switch(role)
    {
        case RoleCurrency:
            {
                const QString& currency_id = m_items.at(index.row()).first;
                result = QVariant::fromValue(Database::getInstance()->getCurrency(currency_id));
            }
            break;
        case RoleVolume:
            {
                const double volume = m_items.at(index.row()).second;
                const int precision = ((volume > 10) || (volume < -10)) ? 2 : 5;
                result = QVariant::fromValue(QString::number(volume, 'f', precision));
            }
            break;
        default:
            qDebug() << "Role" << role << "is not supported";
            break;
    }

    return result;
}

void Portfolio::handleTradesChanged()
{
    beginRemoveRows(QModelIndex(), 0, (m_items.size() - 1U));
    m_items.clear();
    endRemoveRows();

    for(const Trade* trade : Database::getInstance()->getTrades())
    {
        addVolume(trade->inputCurrencyId(), -1 * (trade->inputVolume() + trade->fee()));
        addVolume(trade->outputCurrencyId(), trade->outputVolume());
    }

    beginInsertRows(QModelIndex(), 0, (m_items.size() - 1U));
    endInsertRows();
}

void Portfolio::addVolume(const QString& id, const double volume)
{
    for(auto& item : m_items)
    {
        if(id == item.first)
        {
            item.second += volume;
            return;
        }
    }

    m_items.push_back(qMakePair(id, volume));
}
