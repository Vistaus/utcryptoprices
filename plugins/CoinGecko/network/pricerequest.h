/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PRICEREQUEST_H
#define PRICEREQUEST_H

#include "request.h"

#include <QStringList>

class PriceRequest: public Request
{
public:
    PriceRequest();

    void setCurrencyIds(const QStringList& currency_ids);
    void setVsCurrencyId(const QString& vs_currency_id);
    const QNetworkRequest& request() override;
    double price(const QString& currency_id) const;
    double priceShift(const QString& currency_id) const;

private:
    QStringList m_currency_ids;
    QString m_vs_currency_id;
};

#endif // PRICEREQUEST_H
