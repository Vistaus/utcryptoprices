/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "request.h"

Request::Request(const QString& url_template)
    : m_finished(true)
    , m_url_template(url_template)
    , m_request()
    , m_response()
{
    // intentionally empty
}

bool Request::finished() const
{
    return m_finished;
}

QString Request::toString() const
{
    return m_request.url().toString();
}

void Request::setResponse(const QByteArray& response)
{
    m_response = QJsonDocument::fromJson(response);
    m_finished = true;
}
