/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HISTORYREQUEST_H
#define HISTORYREQUEST_H

#include "request.h"

#include <QList>
#include <QPair>
#include <QDateTime>

class HistoryRequest: public Request
{
public:
    HistoryRequest();

    void setCurrencyId(const QString& currency_id);
    void setVsCurrencyId(const QString& vs_currency_id);
    const QNetworkRequest& request() override;
    QList<QPair<QDateTime, double>> priceHistory() const;

private:
    QString m_currency_id;
    QString m_vs_currency_id;
};

#endif // HISTORYREQUEST_H
