/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tradepair.h"
#include "database/currency.h"
#include "network/historyrequest.h"

TradePair::TradePair(Currency* const currency, Currency* const vs_currency)
    : QObject()
    , m_downloading(false)
    , m_currency(currency)
    , m_vs_currency(vs_currency)
    , m_last_update()
    , m_price(0.0)
    , m_price_shift(0.0)
    , m_network_manager()
    , m_history()
{
    connect(&m_network_manager, &NetworkManager::historyRequestFinished, this, &TradePair::handleHistoryRequestFinished);
}

void TradePair::downloadHistory()
{
    if(!downloading())
    {
        setDownloading(m_network_manager.sendHistoryRequest(m_currency->id(), m_vs_currency->id()));
    }
    else
    {
        qDebug() << "Already downloading";
    }
}

QDateTime TradePair::timeAt(const int index) const
{
    return m_history.at(index).first;
}

double TradePair::priceAt(const int index) const
{
    return m_history.at(index).second;
}

QString TradePair::fixedPriceAt(const int index) const
{
    return QString::number(m_history.at(index).second, 'f', QLocale::FloatingPointShortest);
}

bool TradePair::downloading() const
{
    return m_downloading;
}

void TradePair::setDownloading(const bool downloading)
{
    if(downloading != m_downloading)
    {
        m_downloading = downloading;
        emit downloadingChanged();
    }
}

Currency* TradePair::currency() const
{
    return m_currency;
}

Currency* TradePair::vsCurrency() const
{
    return m_vs_currency;
}

void TradePair::setVsCurrency(Currency* const currency)
{
    if(currency != m_vs_currency)
    {
        m_vs_currency = currency;
        emit vsCurrencyChanged();
    }
}

const QDateTime& TradePair::lastUpdate() const
{
    return m_last_update;
}

void TradePair::setLastUpdateToNow()
{
    m_last_update = QDateTime::currentDateTime();
    emit lastUpdateChanged();
}

double TradePair::price() const
{
    return m_price;
}

QString TradePair::fixedPrice() const
{
    return QString::number(m_price, 'f', QLocale::FloatingPointShortest);
}

void TradePair::setPrice(const double price)
{
    if(price != m_price)
    {
        m_price = price;
        emit priceChanged();
    }
}

double TradePair::priceShift() const
{
    return m_price_shift;
}

void TradePair::setPriceShift(const double price_shift)
{
    if(price_shift != m_price_shift)
    {
        m_price_shift = price_shift;
        emit priceShiftChanged();
    }
}

const QList<QPair<QDateTime, double>>& TradePair::history() const
{
    return m_history;
}

void TradePair::setHistory(const QList<QPair<QDateTime, double>>& history)
{
    m_history = history;
    emit historyChanged();
}

int TradePair::historyLength() const
{
    return m_history.size();
}

void TradePair::handleHistoryRequestFinished(HistoryRequest* const request)
{
    qDebug() << "History request finished";

    setHistory(request->priceHistory());

    setDownloading(false);
}
