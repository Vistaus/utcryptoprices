/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "currencies.h"
#include "database/database.h"
#include "database/currency.h"

#include <QDebug>

Currencies::Currencies()
    : QAbstractListModel()
{
    // intentionally empty
}

Currency* Currencies::at(const int index) const
{
    return Database::getInstance()->getCurrencies().at(index);
}

Currency* Currencies::get(const QString& id) const
{
    return Database::getInstance()->getCurrency(id);
}

int Currencies::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return Database::getInstance()->getCurrencies().size();
}

QHash<int, QByteArray> Currencies::roleNames() const
{
    QHash<int, QByteArray> result;

    result[RoleCurrency] = "currency";

    return result;
}

QVariant Currencies::data(const QModelIndex& index, const int role) const
{
    QVariant result;

    switch(role)
    {
        case RoleCurrency:
            result = QVariant::fromValue(at(index.row()));
            break;
        default:
            qDebug() << "Role" << role << "is not supported";
            break;
    }

    return result;
}
