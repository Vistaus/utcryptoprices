/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PORTFOLIO_H
#define PORTFOLIO_H

#include <QAbstractListModel>
#include <QString>
#include <QList>
#include <QPair>

class QDate;

class Portfolio: public QAbstractListModel
{
    Q_OBJECT

public:
    enum Role
    {
        RoleCurrency,
        RoleVolume
    };

    Portfolio();

    Q_INVOKABLE void initialize();

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex& index, const int role = Qt::DisplayRole) const override;

private slots:
    void handleTradesChanged();

private:
    void addVolume(const QString& id, const double volume);

    QList<QPair<QString, double>> m_items; // currency ID and the owned volume
};

#endif // PORTFOLIO_H
