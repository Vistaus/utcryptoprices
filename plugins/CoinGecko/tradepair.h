/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRADEPAIR_H
#define TRADEPAIR_H

#include "network/networkmanager.h"

#include <QObject>
#include <QString>
#include <QList>
#include <QPair>
#include <QDateTime>

class Currency;
class HistoryRequest;

class TradePair: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool downloading READ downloading NOTIFY downloadingChanged)
    Q_PROPERTY(Currency* currency READ currency CONSTANT)
    Q_PROPERTY(Currency* vsCurrency READ vsCurrency WRITE setVsCurrency NOTIFY vsCurrencyChanged)
    Q_PROPERTY(QDateTime lastUpdate READ lastUpdate NOTIFY lastUpdateChanged)
    Q_PROPERTY(QString price READ fixedPrice NOTIFY priceChanged)
    Q_PROPERTY(double priceShift READ priceShift NOTIFY priceShiftChanged)
    Q_PROPERTY(int historyLength READ historyLength NOTIFY historyChanged)

public:
    TradePair(Currency* const currency, Currency* const vs_currency);

    Q_INVOKABLE void downloadHistory();
    Q_INVOKABLE QDateTime timeAt(const int index) const;
    Q_INVOKABLE double priceAt(const int index) const;
    Q_INVOKABLE QString fixedPriceAt(const int index) const;

    bool downloading() const;
    void setDownloading(const bool downloading);
    Currency* currency() const;
    Currency* vsCurrency() const;
    void setVsCurrency(Currency* const currency);
    const QDateTime& lastUpdate() const;
    void setLastUpdateToNow();
    double price() const;
    QString fixedPrice() const;
    void setPrice(const double price);
    double priceShift() const;
    void setPriceShift(const double price_shift);
    const QList<QPair<QDateTime, double>>& history() const;
    void setHistory(const QList<QPair<QDateTime, double>>& history);
    int historyLength() const;

signals:
    void downloadingChanged();
    void vsCurrencyChanged();
    void lastUpdateChanged();
    void priceChanged();
    void priceShiftChanged();
    void historyChanged();

private slots:
    void handleHistoryRequestFinished(HistoryRequest* const request);

private:
    bool m_downloading;
    Currency* m_currency;
    Currency* m_vs_currency;
    QDateTime m_last_update;
    double m_price;
    double m_price_shift;
    NetworkManager m_network_manager;
    QList<QPair<QDateTime, double>> m_history;
};

#endif // TRADEPAIR_H
