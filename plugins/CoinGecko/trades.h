/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRADES_H
#define TRADES_H

#include <QAbstractListModel>

class Trade;
class QUrl;

class Trades: public QAbstractListModel
{
    Q_OBJECT

public:
    enum Role
    {
        RoleTrade
    };

    Trades();

    Q_INVOKABLE Trade* at(const int index) const;
    Q_INVOKABLE Trade* get(const int id) const;
    Q_INVOKABLE void addTrade(const QString& input_currency_id,
                              const double input_volume,
                              const QString& output_currency_id,
                              const double output_volume,
                              const double fee,
                              const QDate& date,
                              const QString& note);
    Q_INVOKABLE void updateTrade(const int trade_id,
                                 const QString& input_currency_id,
                                 const double input_volume,
                                 const QString& output_currency_id,
                                 const double output_volume,
                                 const double fee,
                                 const QDate& date,
                                 const QString& note);
    Q_INVOKABLE void deleteTrade(const int trade_id);
    Q_INVOKABLE bool importTrades(const QUrl& file_url);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex& index, const int role = Qt::DisplayRole) const override;
};

#endif // TRADES_H
