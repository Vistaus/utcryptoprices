/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "trades.h"
#include "database/database.h"
#include "database/trade.h"

#include <QDebug>
#include <QUrl>
#include <QFile>
#include <QByteArray>

Trades::Trades()
    : QAbstractListModel()
{
    // intentionally empty
}

Trade* Trades::at(const int index) const
{
    return Database::getInstance()->getTrades().at(index);
}

Trade* Trades::get(const int id) const
{
    return Database::getInstance()->getTrade(id);
}

void Trades::addTrade(const QString& input_currency_id,
                      const double input_volume,
                      const QString& output_currency_id,
                      const double output_volume,
                      const double fee,
                      const QDate& date,
                      const QString& note)
{
    Database* db = Database::getInstance();
    const bool result = db->insertTrade(input_currency_id, input_volume, output_currency_id, output_volume, fee, date, note);

    if(result)
    {
        const int count = db->getTrades().size();

        beginRemoveRows(QModelIndex(), 0, count - 2);
        endRemoveRows();

        beginInsertRows(QModelIndex(), 0, count - 1);
        endInsertRows();
    }
}

void Trades::updateTrade(const int trade_id,
                         const QString& input_currency_id,
                         const double input_volume,
                         const QString& output_currency_id,
                         const double output_volume,
                         const double fee,
                         const QDate& date,
                         const QString& note)
{
    Database* db = Database::getInstance();
    const bool result = db->updateTrade(trade_id, input_currency_id, input_volume, output_currency_id, output_volume, fee, date, note);

    if(result)
    {
        const int count = db->getTrades().size();

        beginRemoveRows(QModelIndex(), 0, count - 1);
        endRemoveRows();

        beginInsertRows(QModelIndex(), 0, count - 1);
        endInsertRows();
    }
}

void Trades::deleteTrade(const int trade_id)
{
    Database* db = Database::getInstance();
    const bool result = db->deleteTrade(trade_id);

    if(result)
    {
        const int count = db->getTrades().size();

        beginRemoveRows(QModelIndex(), 0, count);
        endRemoveRows();

        beginInsertRows(QModelIndex(), 0, count - 1);
        endInsertRows();
    }
}

bool Trades::importTrades(const QUrl& file_url)
{
    bool result = false;
    QFile file(file_url.path());

    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        file.readLine(); // header

        while(!file.atEnd())
        {
            const QByteArray line = file.readLine().trimmed();
            const QList<QByteArray> values = line.split(',');

            if(values.size() == 7)
            {
                if(Database::getInstance()->getCurrency(values[2]))
                {
                    if(Database::getInstance()->getCurrency(values[4]))
                    {
                        addTrade(values[2], values[1].toDouble(),
                                 values[4], values[3].toDouble(),
                                 values[5].toDouble(), QDate::fromString(values[0], Qt::ISODate), values[6]);
                        result = true;
                    }
                    else
                    {
                        qDebug() << values[4] << "is not a valid currency ID";
                    }
                }
                else
                {
                    qDebug() << values[2] << "is not a valid currency ID";
                }
            }
            else
            {
                qDebug() << line << "has" << values.size() << "columns instead of 7";
            }
        }
    }
    else
    {
        qDebug() << "Failed to open" << file_url.path();
    }

    return result;
}

int Trades::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return Database::getInstance()->getTrades().size();
}

QHash<int, QByteArray> Trades::roleNames() const
{
    QHash<int, QByteArray> result;

    result[RoleTrade] = "trade";

    return result;
}

QVariant Trades::data(const QModelIndex& index, const int role) const
{
    QVariant result;

    switch(role)
    {
        case RoleTrade:
            result = QVariant::fromValue(at(index.row()));
            break;
        default:
            qDebug() << "Role" << role << "is not supported";
            break;
    }

    return result;
}
