/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CURRENCIES_H
#define CURRENCIES_H

#include <QAbstractListModel>
#include <QString>

class Currency;

class Currencies: public QAbstractListModel
{
    Q_OBJECT

public:
    enum Role
    {
        RoleCurrency
    };

    Currencies();

    Q_INVOKABLE Currency* at(const int index) const;
    Q_INVOKABLE Currency* get(const QString& id) const;

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex& index, const int role = Qt::DisplayRole) const override;
};

#endif // CURRENCIES_H
